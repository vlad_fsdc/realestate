<?php

namespace Drupal\rls_footer_form\Form;

use Drupal\Core\Form\FormStateInterface; //FormBuilderInterface
use Drupal\Core\Form\FormBase;

class FooterForm extends FormBase
{
  /* return a string that is unique Id of form. */
  public function getFormId()
  {
    return 'rls_footer_form';
  }

  /* 
  * Declaring Form
  */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $form['title'] = [
      '#type' => 'item',
      '#markup' => $this->t('QUICK CONTACT'),
    ];
    $form['description'] = [
      '#type' => 'item',
      '#markup' => $this->t('Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the'),
    ];

    //email
    $form['email'] = [
      '#type' => 'textfield',
      '#placeholder' => 'Type your E-mail address...',
      '#required' => TRUE,
    ];

    // Textfield.
    $form['message'] = [
      '#type' => 'textarea',
      '#maxlength' => 1000,
      '#placeholder' => 'Write here...',
    ];

    $form['actions']['#type'] = 'actions';
    // Add a submit button
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
    ];
    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    parent::validateForm($form, $form_state);
    $value = $form_state->getValue('email');
    $valueMessage = $form_state->getValue('message');
    if(strlen($valueMessage) < 3) {
      $form_state->setErrorByName(
        'message',
        $this->t('Your message is too short')
      );
    }
    if (!\Drupal::service('email.validator')->isValid($value) || !filter_var($value, FILTER_VALIDATE_EMAIL)) {
      $form_state->setErrorByName('email', $this->t('Email address is not a valid one.'));
    }
  }

  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    //\Drupal::service('plugin.manager.mail')->mail($module, $key, $to, $langcode);
    $module = 'rls_footer_form';
    $to = \Drupal::config('system.site')->get('mail');
    $from = $form_state->getValue('email');
    $params['message'] = $form_state->getValue('message');

    \Drupal::service('plugin.manager.mail')->mail($module, '', $to, 'en',['subject' => '', 'body' => $params['message'],'from_mail' => $from]);

    $this->messenger()->addStatus($this->t('Your message is @message', ['@message' => $form_state->getValue('message')]));
    $this->messenger()->addStatus($this->t('Your email is @email', ['@email' => $form_state->getValue('email')])); 
    
  }
}
