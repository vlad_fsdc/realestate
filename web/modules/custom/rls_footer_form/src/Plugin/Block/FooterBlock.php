<?php

namespace Drupal\rls_footer_form\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;



/**
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "rls_footer_block_block",
 *   admin_label = @Translation("FooterBlock"),
 * )
 */
class FooterBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = \Drupal::formBuilder()->getForm('Drupal\rls_footer_form\Form\FooterForm');

    return $form;
  }

  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

}