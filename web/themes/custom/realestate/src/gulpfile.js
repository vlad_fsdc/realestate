
//Connect the gulp modules
const gulp = require('gulp');
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');

const sourcemaps = require('gulp-sourcemaps');
const sass = require('gulp-sass')(require('sass'));

// Connection order of the sass/scss-files
const cssFiles = [
  './sass/styles.scss',
];

//Task for CSS styles
function styles() {
  return gulp.src(cssFiles)
    .pipe(sourcemaps.init())
    .pipe(sass())
    //Merge files into one
    .pipe(concat('style.css'))
    //Add prefixes
    .pipe(autoprefixer())
    //CSS minification
    .pipe(cleanCSS({
      level: 2
    }))
    .pipe(sourcemaps.write('./'))
    //Output folder for styles
    .pipe(gulp.dest('./css'))
}


//Watch files
function watch() {
  //Watch CSS files
  gulp.watch('./sass/**/*.scss', styles);
}

//Task calling 'styles' function
gulp.task('styles', styles);
//Task for changes tracking
gulp.task('watch', watch);
