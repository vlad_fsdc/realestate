//-------------------------------------BURGER BUTTON-------------------------------------//

const menu = document.querySelector(".menu--main");
const menuBtn = document.querySelector(".menu-main__responsive-btn");

menuBtn.addEventListener('click', function() {
    menu.classList.toggle('expanded');
});

//-------------------------------------FIXED MENU-------------------------------------//

let scrollTop = window.scrollY;
const menuDistance = menu.offsetTop;

document.addEventListener('scroll', () => {
    scrollTop = window.scrollY;

    if ( scrollTop > menuDistance ) {
        menu.classList.add('fixed');
    } else {
        menu.classList.remove('fixed');
    }
})

//-------------------------------------FIXED MENU PROPERTIES-------------------------------------//

const open = document.querySelector(".dropdown-menu");
const openBtn = document.querySelector(".plus");

openBtn.addEventListener('click', function() {
    open.classList.toggle('expanded');
});